// Création de la carte de fond à partir des tuiles de Esri
var map = L.map('map').setView([0, 0], 1);

var Esri_WorldImagery = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
    attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
}).addTo(map);

// Fonctions fournies pour récupérer les tuiles correspondant à la zone survolée par l'ISS
function getTile (latlng, zoom) {
    let xy = project(latlng[0], latlng[1], zoom);
    return {
      x: xy.x,
      y: xy.y,
      z: zoom
    };
}
  
function project (lat, lng, zoom) {
    const R = 6378137;
    let sphericalScale = 0.5 / (Math.PI * R);
    let d = Math.PI / 180,
        max = 1 - 1E-15,
        sin = Math.max(Math.min(Math.sin(lat * d), max), -max),
        scale = 256 * Math.pow(2, zoom);
  
    let point = {
      x: R * lng * d,
      y: R * Math.log((1 + sin) / (1 - sin)) / 2
    };
  
    point.x = tiled(scale * (sphericalScale * point.x + 0.5));
    point.y = tiled(scale * (-sphericalScale * point.y + 0.5));
  
    return point;
}
  
function tiled (num) {
    return Math.floor(num / 256);
}

// Fonction pour créer les orbites de l'ISS
function connectTheDots(data,latitude,longitude){
    var sup =[];
    var inf =[];
    for(i in data) {
        var x = data[i].lat;
        var y = data[i].lng;
        if (y<longitude-10 && (x>=(latitude+1) || x<=(latitude-1))) {
            inf.push([x, y])
        } else {
            if(y>=(longitude-10)){
            sup.push([x, y])}
        }
    }
    // console.log(c) //debugtest
    sup.sort((a, b) => (a[1] < b[1] ? 1 : -1))
    // console.log(sup) //debugtest
    inf.sort((a, b) => (a[1] < b[1] ? 1 : -1))
    // console.log(inf) //debugtest
    return [sup, inf];
}


// Framework Vue.js et éléments devant se mettre à jours lors des différentes requêtes AJAX
Vue.createApp({
    data() {
        return {
            latitude: 0,
            longitude: 0,
            followISS: false,
            marker: null,
            orbitLayer: null,

            zoom:0,
            tile:'',
            image:'',
            loca:'',
            tweet:'# Hello World !!!',
            url:'',
        };
    },
    mounted() {
        // Actions se produisant au démarrage
        this.updateISSLocation();
        orbit = new L.LayerGroup().addTo(map);

    },
    methods: {

        // Récupération des coordonnées de l'ISS 
        updateISSLocation() {
            fetch('http://api.open-notify.org/iss-now.json')
            .then(response => {
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }
                return response.json();
            })
            .then(data => {
                const { latitude, longitude } = data.iss_position;
                this.latitude = latitude;
                this.longitude = longitude;
                this.updateMarkerPosition([latitude, longitude]);
                this.updateISSOrbit(latitude,longitude)
                setTimeout(this.updateISSLocation, 500);
            })
            .catch(error => {
                console.error('Error fetching ISS location:', error);
            });
        },

        // Mise à jour de la position du marqueur
        updateMarkerPosition(position) {
            const icon = L.icon({
                iconUrl: 'https://upload.wikimedia.org/wikipedia/commons/f/f2/ISS_spacecraft_model_1.png',
                iconSize: [100, 50],
                iconAnchor: [50, 25]
            });
            if (this.marker) {
                this.marker.setLatLng(position);
            } else {
                this.marker = L.marker(position, { icon: icon }).addTo(map);
            }
            if (this.followISS) {
                map.setView(position);
            }
        },

        // Forcer l'affichage de la carte à suivre l'ISS ou navigation libre
        toggleMapFollow() {
            const Label = document.querySelector('label[for="followISS"]');
            if (this.followISS && this.marker) {
                const { lat, lng } = this.marker.getLatLng();
                map.setView([lat, lng], 4);
                map.dragging.disable();
                Label.classList.add('locked');
            } else {
                map.dragging.enable();
                Label.classList.remove('locked');
            }
        },

        // Récupération et affichage de l'orbite de l'ISS
        updateISSOrbit(latitude,longitude) {
            fetch('https://dev.iamvdo.me/orbit.php')
            .then(response => {
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }
                return response.json();
            })
            .then(data => {
                // console.log(data) //debugtest   
                [sup, inf]= connectTheDots(data,latitude,longitude);
                var pathLine_sup = L.polyline(sup)
                var pathLine_inf = L.polyline(inf)

                if (sup.length>0){
                    orbit.clearLayers();
                    pathLine_sup.addTo(orbit);
                    pathLine_inf.addTo(orbit);}
                
            })
            .catch(error => {
                console.error('Error fetching ISS orbit:', error);
            });
        },

        // Fonction pour les tweets et l'affichage de la tuile survolée par l'ISS à différents niveaux de zoomp
        submit (){
            p=Math.floor(Math.random() * 3);
           
            // Coordonnées de test
            // Océans
              // lat=37.8295989
              // long=-32.7640058
            // Pas de villes
              // lat=-25,4952930
              // long=128,9061443
            // Villes
              // lat=6.9753500
              // long=158.1920843
            // Paris - 12e
            //   lat=48.85
            //   long=2.35
            // Coordonnées de test
  
              this.tile=getTile([this.latitude, this.longitude], this.zoom)
              // console.log(this.tile["x"]) //debugtest 
              this.image='https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/'+this.tile["z"]+'/'+this.tile["y"]+'/'+this.tile["x"]
              // console.log(this.image) //debugtest 
  
              //  Version 1
              // this.Recherche='http://api.geonames.org/findNearbyPlaceNameJSON?lat='+lat+'&lng='+long+'&username=archgeo'
              // // console.log(this.Recherche) //debugtest 
              // fetch(this.Recherche)
              // .then(result => result.json())
              // .then(result => {
              //   // console.log(result) //debugtest
              //   // console.log(result.geonames) //debugtest
              // if(result.geonames!=undefined){
                //   this.tweet=' Hello '+result.geonames[0].toponymName+' #'+result.geonames[0].countryName}
                // else {this.tweet='# Hello World !!!'}
              //   })
  
              // Version 2
              url='http://api.geonames.org/extendedFindNearby?lat='+this.latitude+'&lng='+this.longitude+'&username=archgeo'
              // console.log(url) //debugtest 
              // Préparer la variable
              let recherche = new FormData();
              // Mot clef + valeur dans les parenthèses
              recherche.append('recherche', url);
              // console.log(recherche) //debugtest 
              fetch('/oceans', {
                  method: 'post',
                  body: recherche,
                })
                .then(r => r.json())
                .then(r => {
                  r=JSON.parse(r)
                  // console.log(r); //debugtest 
                  
                  if (r["ocean"]!=undefined){
                    // console.log(r["ocean"].name); //debugtest 
                    oceans=['Nous survollons actuellement #'+r["ocean"].name+" ! Ça en fait de l'eau !",
                            'Je me demande à quoi ressemble les poissons de #'+r["ocean"].name+' ?',
                            "Est-ce le ciel ? Un verre d'eau ? Non, c'est #"+r["ocean"].name+' !'];
                    // console.log(oceans[p]) //debugtest 
                    this.tweet=oceans[p]
                  }
                  else{ 
                    // console.log(2) //debugtest 
                    if(r["countryName"]!=undefined){
                    // console.log(r["countryName"]); //debugtest 
                      pays=['Nous survollons actuellement le #'+r["countryName"]+" ! Et pas une ville en vue...?",
                            'Quelle belle vue de #'+r["countryName"]+" ! C'est à se demander ce que ça donne sur la terre ferme !",
                            'Enfin arrivé en #'+r["countryName"]+" ! Il va falloir en profiter tant que ça dure !"];
  
                      this.tweet=pays[p]
                  }
                    else {
                        if(r["geoname"]!=undefined){
                        // console.log(3) //debugtest 
                        // console.log(r["geoname"].length) /debugtest  
                        n=r["geoname"].length-1
                        villes=["Mais...! C'est "+r["geoname"][n].name+' ! Ça veut dire que nous sommes au dessus de #'+r["geoname"][2].name+' !',
                                "Je crois que c'est "+r["geoname"][n].name+"...! Mais je ne me souviens plus si c'est la capitale de #"+r["geoname"][2].name+'...',
                                "La vue sur "+r["geoname"][n].name+" est imprenable d'ici ! Et #"+r["geoname"][2].name+" semble bien paisible depuis l'espace"];
    
                        this.tweet=villes[p]
                        } else {
                            this.tweet='# Hello World !!!'
                        }
                    }}
                })
        }
    }
}).mount('#app');
