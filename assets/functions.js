function getTile (latlng, zoom) {
  let xy = project(latlng[0], latlng[1], zoom);
  return {
    x: xy.x,
    y: xy.y,
    z: zoom
  };
}

function project (lat, lng, zoom) {
  const R = 6378137;
  let sphericalScale = 0.5 / (Math.PI * R);
  let d = Math.PI / 180,
      max = 1 - 1E-15,
      sin = Math.max(Math.min(Math.sin(lat * d), max), -max),
      scale = 256 * Math.pow(2, zoom);

  let point = {
    x: R * lng * d,
    y: R * Math.log((1 + sin) / (1 - sin)) / 2
  };

  point.x = tiled(scale * (sphericalScale * point.x + 0.5));
  point.y = tiled(scale * (-sphericalScale * point.y + 0.5));

  return point;
}

function tiled (num) {
  return Math.floor(num / 256);
}

// récuperér la tuile du centre de Paris au zoom 15
getTile([48.85, 2.35], 15) // => {x: 16597, y: 11274}