<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="Tweete_commePesquet" content="width=device-width, initial-scale=1.0">
    <title>Tweete comme Pesquet</title>

    <!-- Leaflet CSS -->
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.9.4/dist/leaflet.css">
    <link rel="stylesheet" href="/assets/app.css">

    <!-- Google Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Orbitron&display=swap">
</head>

<body>

    <h1>Tweete comme Pesquet</h1>
    <div id="map"></div>
    <div id="app">
        <p class="coordinates">Position de l’ISS en temps réel</p>
        <p class="coordinates">Latitude: {{ latitude }}</p>
        <p class="coordinates">Longitude: {{ longitude }}</p>
        <input type="checkbox" id="followISS" v-model="followISS" @change="toggleMapFollow">
        <label for="followISS">Suivre l'ISS</label>
        <h2>Choix du zoom de la photo :</h2>
            <form action="" @submit.prevent="submit" >
            <div>
                <input type="radio"name="smartphone" value="7" v-model="zoom"/>
                <label for="smartphone">Smartphone</label>
                <input type="radio" name="reflex" value="10" v-model="zoom"/>
                <label for="reflex">Réflex</label>
                <input type="radio" name="teleobjectif" value="13" v-model="zoom"/>
                <label for="teleobjectif">Téléobjectif</label>
            </div>
            <div>
                <button type="submit">Tweete comme Pesquet !</button>
            </div>
        </form>
        <div class="texte">
            <h3>{{tweet}}</h3>
            
        </div>
        <div class="photo">
            <img :src="image">
        </div>
    </div>


    <div id="background-container"></div>

    <script src="https://cdn.jsdelivr.net/npm/vue"></script>
    <script src="https://unpkg.com/leaflet@1.9.4/dist/leaflet.js"></script>
    <script src="/assets/app.js"></script>
</body>
<footer><p>Qiuling LIN et Thomas ANDRE - DM WEB</p></footer>
</html>
