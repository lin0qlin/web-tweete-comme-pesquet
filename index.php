<?php
session_start();
require 'flight/Flight.php';

Flight::route('/', function() {
  Flight::render('mainpage');
});

Flight::route('POST /oceans', function() {
  if (isset($_POST['recherche'])) {
    $url=$_POST['recherche'];
    $xml=simplexml_load_file($url);
    $recherche=json_encode($xml);
  };
  Flight::json($recherche);
});

Flight::start();
?>